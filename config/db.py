#import json
import os
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
#file = open("db/data.json")
#data = json.load(file)
#file.close()

sqlite_file_name = "../database.sqlite"
base_dir = os.path.dirname(os.path.realpath(__file__))

"""
    Dirname()
    El método os.path.dirname() en Python se usa para obtener el nombre del directorio de la ruta especificada.
    
    Realpath()
    El método os.path.realpath() en Python se usa para obtener la ruta canónica (por así decirlo, la URL original de una web) del nombre de archivo especificado al eliminar cualquier enlace simbólico que se encuentre en la ruta.
    
    Join()
    Combina nombres de rutas en una ruta completa. Esto significa que puede fusionar varias partes de una ruta en una, en lugar de codificar manualmente cada nombre de ruta.
"""

database_url = f"sqlite:///{os.path.join(base_dir,sqlite_file_name)}" #sqlite:/// #es la forma en la que se conecta a una base de datos, se usa el metodo join para unir las urls

engine = create_engine(database_url,echo=True)#representa el motor de la base de datos, con el comando “echo=True” para que al momento de realizar la base de datos,me muestre por consola lo que esta realizando, que seria el codigo

Session = sessionmaker(bind=engine) #Se crea session para conectarse a la base de datos, se enlaza con el comando “bind” y se iguala a engine

Base = declarative_base() #Sirve para manipular todas las tablas de la base de datos