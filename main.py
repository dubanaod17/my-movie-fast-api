from fastapi.responses import HTMLResponse,JSONResponse
import uvicorn
from config.db import engine,Base
from  middleware.error_handler import ErrorHandler
from routers.movie import movie_router
from routers.users import login_user_router
from fastapi import FastAPI


#from models import Movie,User
#import db --> es una opcion para manejar el diccionario de peliculas.

app = FastAPI()

app.title = 'Aprendiendo FastApi'
app.version = "0.0.1"
app.contact =  {
        "name": "Duban Oviedo",
        "url": "https://OviDaz17.github.io/portfolio/",
        "email": "duban.oviedo@correo.uis.edu.co"
    }
#la documentación del API es gestionada por swagger (documentacion autogenerada)

app.add_middleware(ErrorHandler)#llamada al middleware que se ejecuta cuando hay un error en la apliación

app.include_router(movie_router)

app.include_router(login_user_router)

Base.metadata.create_all(bind=engine)


#movies = [
#     {
#		"id": 1,
#		"title": "Avatar",
#		"overview": "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
#		"year": "2009",
#		"rating": 7.8,
#		"category": "Acción"
#	},
#   {
#		"id": 2,
#		"title": "Fast & Furious",
#		"overview": "es una franquicia de medios estadounidense centrada en una serie......",
#		"year": "2008",
#		"rating": 8,
#		"category": "Acción"
#	}
#]

#def valid_querie(year:str,category:str,movie:list):
#    x = []
#    for i in movie:
#        if i['category'] == category and i['year'] == year:
#            x.append(i)
#    return x

#Se crea el primer endpoint --> dentro se indica la ruta de inicio / tags, se utiliza para agrupar determinadas rutas en la aplicación
@app.get('/', tags = ['HOME']) # home  es la etiqueta de la ruta, sirve para agruparlas
def message():
    return HTMLResponse('<h1>Hello World!<h1>') #Función que se va a ejecutar.
# la función puede devolver lor que querramos, desde un string, un integer, un html, un dictionary ej: {"hello":"world"}


#http://127.0.0.1:8000/
if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
