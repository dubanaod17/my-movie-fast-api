from starlette.middleware.base import BaseHTTPMiddleware 
from fastapi import FastAPI,Request,Response
from fastapi.responses import JSONResponse

class ErrorHandler(BaseHTTPMiddleware):
    """
    Clase creada para el manejo de errores
    """
    def __init__(self, app:FastAPI) -> None: #objeto constructor
        super().__init__(app)
        
    async def dispatch(self, request: Request,call_next) -> Response | JSONResponse: #metodo que se ejecuta frecuentemente para ver si hay algún error en la app, requiere un metodo request para acceder a todas las peticiones que se estan ejecutando en la aplicacion
        try: 
            return await call_next(request) #call_next representa la siguiente llamada
        except Exception as e:
            return JSONResponse(status_code=500, content={'error': str(e)})
        