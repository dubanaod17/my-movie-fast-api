from fastapi import APIRouter
from fastapi import Path, Query,Depends #Clases 
from fastapi.responses import JSONResponse
from typing import List
from config.db import Session
from models.movie import Movie as MovieModel#el apodo es porque ya hay una variable con ese nombre
from fastapi.encoders import jsonable_encoder
from middleware.jwt_bearer import JWTBearer
from services.movie import MovieService
from schemas.movie import Movie


movie_router = APIRouter() # es como crear una aplicacion, pero ahora se hace a nivel de router


#metodo get que devuelve las peliculas disponibles
@movie_router.get('/movies', tags = ['movies'], response_model= List[Movie], status_code=200, dependencies=[Depends(JWTBearer())])
def get_movies() -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(status_code=200,content=jsonable_encoder(result))#le dejo claro a Fastapi que respuesta entregar JSONResponse(status_code=200,content=movies)

#para indicarle a una ruta que va a requerir de parametros ej {id}
@movie_router.get('/movies/{id}',tags = ['movies'], response_model= Movie)
def get_movie(id: int = Path(ge=1, le=2000))-> Movie: #path es la clase que me valida el parametro
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404,content={'message': 'not found'})
    #movie = next(filter(lambda x: x["id"] == id, movies), JSONResponse(content=[],status_code=404)) 
    return JSONResponse(status_code=200,content=jsonable_encoder(result))


#parametros query / cuando se le indica a fastapi que la función va a requerir de un parametro y no se le indica en la ruta automaticamente lo lee como un parametro query
@movie_router.get('/movies/', tags = ['movies'],response_model= List[Movie])
#para agregar otro parametro basta con añadirlo a la funcion
def get_movies_by_query(category:str = Query(min_length=5,max_length=15) ,year:str=None)->List[Movie]:#la validacion de parametros en query es aplicada con la clase Query
    db = Session()
    result = MovieService(db).get_movies_by_query
    if not result:
        return JSONResponse(content={'message': 'not found'},status_code=404)
    #movie = next(filter(lambda x: x["id"] == id, movies), JSONResponse(content=[],status_code=404)) 
    return JSONResponse(status_code=200,content=jsonable_encoder(result))
    
@movie_router.post('/movies', tags = ['movies'], response_model=dict,status_code=201)# el estatus 201 es para dejar constancia de que el registro fue exitoso
def create_movie(movie:Movie) -> dict : #create_movie(id:int = Body(), title:str = Body(),overview:str = Body(),year:int = Body(),rating:float = Body(), category:str = Body()):
    db = Session()
    MovieService(db).create_movie(movie)
    #movies.append(movie)
        #movies.append({
        #"id" : id,
        #"title" : title,
        #"overview" : overview,
        #"year" : year,
        #"rating" : rating,
        #"category" : category
    #})
    return  JSONResponse(status_code=201,content={"message": "Se ha registrado la pelicula"}) #movies

@movie_router.put('/movies/{id}',tags = ['movies'], response_model=dict,status_code=200)
def update_movie(id:int, movie:Movie)-> dict: #update_movie(id:int,title:str = Body(),overview:str = Body(),year:int = Body(),rating:float = Body(), category:str = Body()):
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(content={'message': 'not found'},status_code=404)
    #for i in movies:
        #if i["id"] == id: 
            #i["title"] = movie.title #i[title] = title
            #i["overview"] = movie.overview #i[overview] = overview
            #i["year"] = movie.year#i[year] = year
            #i["rating"] = movie.rating#i[rating] = rating
            #i["category"] = movie.category #i[category] = category
    MovieService(db).update_movie(id,movie)
    return JSONResponse(status_code=200,content={"message": "Se ha modificado la pelicula"}) #movies

@movie_router.delete('/movies/{id}',tags = ['movies'], response_model=dict,status_code=200)
def delete_movie(id:int)-> dict:
    db = Session()
    result: MovieModel = db.query(MovieModel).filter(MovieModel.id == id).first()
    if not result:
        return JSONResponse(status_code=404,content={'message': 'not found'})
    #for i in movies: 
        #if i["id"] == id:
            #movies.remove(i)
    MovieService(db).delete_movie(id)
    return JSONResponse(status_code=200,content={"message": "Se ha eliminado la pelicula"}) #movies




