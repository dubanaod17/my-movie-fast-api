from fastapi import APIRouter
from pydantic import BaseModel
from utils.Jwt_manager import create_token
from fastapi.responses import JSONResponse
from schemas.users import User


login_user_router = APIRouter()

@login_user_router.post('/login', tags=['auth'])
def login(user: User):
    if user.email == "admin@gmail.com" and user.password == "admin":
        token:str = create_token(user.model_dump()) #se usa model_dump en remplazo de .dict() porque fue deprecado
        return JSONResponse(status_code=200,content=token)