from pydantic import BaseModel, Field
from typing import Optional

class Movie(BaseModel): #esta clase se crea para evitar escribir toda la data en la petición post y put 
    id: Optional[int] = None # id: int | None = None 
    title: str = Field(min_length=3, max_length=100) #default="mi pelicula"--> ya no pongo default 
    overview: str = Field(default='Descripcion de la película', min_length=15, max_length=200)
    year: int = Field(default=2022, le=2022)#int
    rating: float = Field(ge=1, le=10)
    category: str = Field(min_length=3, max_length=30)
    
    model_config = { #esto debe llamarse así si no no funciona
        "json_schema_extra": { #esto debe llamarse así si no no funciona
            "examples" : [
                {   #esto debe llamarse así si no no funciona
            	    "id": 1,
		            "title": "Avatar",
		            "overview": "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
		            "year": 2009,
		            "rating": 7.8, 
		            "category": "Acción"
                }
            ]
        }
    }