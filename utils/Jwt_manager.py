from jwt import encode, decode #Libreria para generar tokens de autenticacion
#from dotenv import load_dotenv
#import os
#load_dotenv() esto es para las cargar las contraseñas creadas en variables de entorno.

def create_token(data: dict) -> str:
    token: str = encode(
                        payload=data,
                        key='SECRET_KEY',
                        algorithm="HS256"
                        )
    return token

#def create_token(data: dict):
    #token: str = encode(payload=data, key="my_key_from_babilonia", algorithm="HS256")
    #return token

def validate_token(token:str) -> dict:
    data:dict = decode(token, key='SECRET_KEY', algorithms = ['HS256'])
    return data

#def validate_token(token:str) -> dict:
    #data:dict = decode(token, key="my_secret_key", algorithms = ['HS256'])
    #return data